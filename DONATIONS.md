# Donations

Thank you for considering a donation to this project.

This is a [Bitcoin](http://www.bitcoin.org) only project, and therefore donations are only to be made in Bitcoin.

As this is a collective project, please consider which of the [Authors](http://www.github.com/PartTimeLegend/BitcoinBusinessPack/AUTHORS.md) you wish to donate to and donate directly from there.